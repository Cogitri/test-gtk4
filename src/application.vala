namespace TestRefcycle {
    public class Application : Gtk.Application {
        private weak Window? window;

        public Application () {
            Object (application_id: "dev.Cogitri.test", flags : ApplicationFlags.FLAGS_NONE);
        }


        public override void startup () {
            base.startup ();
            Hdy.init ();
        }

        public override void activate () {
            if (window != null) {
                return;
            } else {
                var w = new Window (this);
                this.window = w;
                this.window.show ();
            }
        }

    }
}
