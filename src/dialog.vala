[GtkTemplate (ui = "/dev/Cogitri/test/dialog.ui")]
public class Dialog : Gtk.Dialog {
    public Dialog (Gtk.Window parent) {
        Object (use_header_bar: 1);
        this.set_transient_for (parent);
    }
}
